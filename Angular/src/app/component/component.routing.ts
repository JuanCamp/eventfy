import { Routes } from '@angular/router';


import { FullComponent } from '../layouts/full/full.component';
import { UsuarioListComponent } from './usuario-list/usuario-list.component';



export const ComponentsRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'listaUsuarios',
        component: UsuarioListComponent
      }
    ]
  }
];
