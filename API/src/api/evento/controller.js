import { success, notFound, authorOrAdmin } from '../../services/response/'
import { Evento } from '.'
import {User} from '../user'
import {Foto} from '../foto'

export const create = ({ user, bodymen: { body } }, res, next) =>
  Evento.create({ ...body, userId: user })
    .then((evento) => evento.view(true))
    .then(success(res, 201))
    .catch(next)

/*
export const index = ({ querymen: { query, select, cursor } }, res, next) =>
Evento.count(query)
.then(count => Evento.find(query, select, cursor)
  .then((eventos) => ({
    count,
    rows: eventos.map((evento) => evento.view())
  }))
)
.then(success(res))
.catch(next)
*/
let queryFirstPhoto = (evento) => {
  return new Promise((resolve, reject) => {
    Foto
      .find({ eventoId: evento.id })
      .sort({ _id: -1 })
      .limit(1)
      .exec(function (err, fotos) {
        if (err) {
          reject(err)
        }
        else {
          let result = JSON.parse(JSON.stringify(evento))
          result['loc'] = evento.loc;
          if (fotos.length > 0)
            result['fotos'] = [fotos[0].imgurLink]
          resolve(result)
        }
      })
  })
}

let queryAllPhotos = (evento) => {
  return new Promise((resolve, reject) => {
    Foto
      .find({ eventoId: evento.id })
      .exec(function (err, fotos) {
        if (err) {
          reject(err)
        }
        else {
          let result = JSON.parse(JSON.stringify(evento))
          result['loc'] = evento.loc;
          let images = fotos.map((foto) => foto.imgurLink)
          result['fotos'] = images
          resolve(result)
        }
      })
  })
}

export const index = ({ params  , querymen: { query, select, cursor } }, res, next) => {
  Evento
    .find(query, select, cursor)
    .populate('userId', 'name picture')
    .populate('categoriaId', 'name')
    .exec(function (err, eventos){
        if (err) next;
        Promise.all(eventos.map(function(evento){
          return queryFirstPhoto(evento)
        }))
        .then((result) => ({
          count: result.length,
          rows: result
        }))
        .then(success(res))
        .catch(next)
    })
}

export const authenticatedIndex = ({ user, querymen: { query, select, cursor } }, res, next) => {
  Evento
    .find(query, select, cursor)
    .populate('userId', 'name picture')
    .populate('categoriaId', 'name')
    .exec(function (err, eventos){
        if (err) next;
        Promise.all(eventos.map(function(evento){
          return queryFirstPhoto(evento)
        }))
        .then((result) => result.map((evento) => {
            let favoriteEvento = JSON.parse(JSON.stringify(evento))
            console.log(user.favs);
            console.log('Id ' + evento.id)
            favoriteEvento['isFav'] = user.favs.indexOf(evento.id) > -1 
            return favoriteEvento
          })
        )
        .then((result) => ({
          count: result.length,
          rows: result
        }))
        .then(success(res))
        .catch(next)
    })
}
   

export const userEventos = ({ user, querymen: { query, select, cursor } }, res, next) => {
    query['userId'] = user.id
    Evento
    .find(query, select, cursor)
    .then((eventos) => ({
      count: eventos.length,
      rows: eventos.map((evento) => evento.view())
    }))
    .then(success(res))
    .catch(next)

  }


  export const show = ({ params }, res, next) =>
    Evento.findById(params.id)
      .populate('userId', 'name picture')
      .populate('categoriaId', 'name')
      .exec(function (err, evento) {
        if (err) next
        if (evento === null) {
          res.status(404).end()
        } else {
          queryAllPhotos(evento)
            .then((result) => ({
              count: 1,
              rows: result
            }))
            .then(success(res))
            .catch(next)
        }
      })

  export const update = ({ user, bodymen: { body }, params }, res, next) =>
    Evento
      .findById(params.id)
      .then(notFound(res))
      .then(authorOrAdmin(res, user, 'userId'))
      .then((evento) => evento ? Object.assign(evento, body).save() : null)
      .then((evento) => evento ? evento.view(true) : null)
      .then(success(res))
      .catch(next)

  export const destroy = ({ user, params }, res, next) =>
    Evento
      .findById(params.id)
      .then(notFound(res))
      .then(authorOrAdmin(res, user, 'userId'))
      .then((evento) => evento ? evento.remove() : null)
      .then(success(res, 204))
      .catch(next)

  export const addFavorite = ({ user, params }, res, next) =>
    User
      .findByIdAndUpdate(user.id, { $addToSet: { favs: params.id } }, { new: true })
      .then(success(res, 200))
      .catch(next)

  export const delFavorite = ({ user, params }, res, next) =>
    User
      .findByIdAndUpdate(user.id, { $pull: { favs: params.id } }, { new: true })
      .then(success(res, 200))
      .catch(next)

      export const userFavorites = ({ user, querymen: { query, select, cursor } }, res, next) => {
        query['_id'] = { $in: user.favs }
        Evento
          .find(query, select, cursor)
          .populate('categoriaId', 'name')
          .populate('userId', 'name picture')
          .exec(function (err, eventos){
              Promise.all(eventos.map(function(evento){
                return queryFirstPhoto(evento)
              }))
              .then((result) => ({
                count: result.length,
                rows: result
              }))
              .then(success(res))
              .catch(next)
          })
      }
