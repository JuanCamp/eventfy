import mongoose, { Schema } from 'mongoose'
import {Foto} from '../foto'

const S = require('string')

const eventoSchema = new Schema({
  userId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  titulo: {
    type: String,
    required: true
  },
  descripcion: {
    type: String
  },
  likes: {
    type: String
  },
  categoriaId: {
    type: Schema.ObjectId,
    ref: 'Categoria'
  },
  direccion: {
    type: String
  },
  provincia: {
    type: String
  },
  loc: {
    type: [Number],
    required: true,
    get: (v) => (v && v.length > 0) ? v.join() : null,
    set: (v) => (S(v).isEmpty()) ? null : v.split(',').map(Number),
  },
  imagenes: {
    type:  [String]
  },
  valoracionMedia: {
    type: String
  },
  comentarios: {
    type: Schema.ObjectId,
    ref: 'Comentario'
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

eventoSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      userId: this.userId,
      titulo: this.titulo,
      descripcion: this.descripcion,
      likes: this.likes,
      categoriaId: this.categoriaId,
      direccion: this.direccion,
      provincia: this.provincia,
      loc: this.loc,
      imagenes: this.imagenes,
      valoracionMedia: this.valoracionMedia,
      comentarios: this.comentarios,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    const viewFull =  {
      id: this.id,
      userId: this.userId,
      titulo: this.titulo,
      descripcion: this.descripcion,
      likes: this.likes,
      categoriaId: this.categoriaId,
      direccion: this.direccion,
      provincia: this.provincia,
      loc: this.loc,
      imagenes: this.imagenes[0],
      valoracionMedia: this.valoracionMedia,
      comentarios: this.comentarios,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...viewFull
      // add properties for a full view
    } : view
  }
}

eventoSchema.pre('remove', {query:true}, function(next){
  Foto    
    .find({eventoId: this.id})
    .exec((err, result) => {
      Promise.all(result.map(foto => foto.remove()))
      next()
    }
    )
})

eventoSchema.index({loc: '2dsphere'})

const model = mongoose.model('Evento', eventoSchema)

export const schema = model.schema
export default model
