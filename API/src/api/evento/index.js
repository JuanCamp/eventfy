import { Router } from 'express'
import { middleware as query, Schema } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy, authenticatedIndex, userEventos, userFavorites, addFavorite, delFavorite, } from './controller'
import { schema } from './model'
export Evento, { schema } from './model'
import {master, token} from '../../services/passport'


const router = new Router()
const { userId, titulo, descripcion, likes, categoriaId, direccion, provincia, loc, imagenes, valoracionMedia, comentarios } = schema.tree

const eventosSchema = new Schema({

  ciudad: {
    type: [String],
    paths: ['ciudad']
  },
  provincia: {
    type: [String],
    paths: ['provincia']
  },
  near: {
    paths: ['loc']    
  }, 
  categoria: {
    type: String,
    paths: [categoriaId]
  },
  direccion: {
    type: RegExp,
    paths: ['direccion']
  },
  
}, {near: true})

/**
 * @api {post} /eventos Create evento
 * @apiName CreateEvento
 * @apiGroup Evento
 * @apiParam userId Evento's userId.
 * @apiParam titulo Evento's titulo.
 * @apiParam descripcion Evento's descripcion.
 * @apiParam likes Evento's likes.
 * @apiParam categoriaId Evento's categoriaId.
 * @apiParam direccion Evento's direccion.
 * @apiParam provincia Evento's provincia.
 * @apiParam loc Evento's loc.
 * @apiParam imagenes Evento's imagenes.
 * @apiParam valoracionMedia Evento's valoracionMedia.
 * @apiParam comentarios Evento's comentarios.
 * @apiSuccess {Object} evento Evento's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Evento not found.
 */
router.post('/',
  token({ required: true}),
  body({ titulo, descripcion, likes, categoriaId, direccion, provincia, loc}),
  create)

/**
 * @api {get} /eventos Retrieve eventos
 * @apiName RetrieveEventos
 * @apiGroup Evento
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of eventos.
 * @apiSuccess {Object[]} rows List of eventos.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  master(),
  query(eventosSchema),
  index)

/**
 * @api {get} /properties/auth/ Retrieve properties when user is authenticated
 * @apiName RetrievePropertiesAuthenticated
 * @apiGroup Property
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam {String} access_token master access token.
 * @apiParam {Number} [rooms] Number of rooms. It can be a list of values (optional)
 * @apiParamExample {Number} Rooms param example
 * rooms=1,2,3
 * @apiParam {String} [city] Name of the city (optional)
 * @apiParam {String} [province] Name of the province (optional)
 * @apiParam {String} [zipcode] Value of the zipcode (optional)
 * @apiParam {Number} [min_size] Min size of the property (optional)
 * @apiParam {Number} [max_size] Max size of the property (optional)
 * @apiParam {Number} [min_price] Min price of the property (optional)
 * @apiParam {Number} [max_price] Max price of the property (optional)
 * @apiParam {String} [category] Category Id of the property
 * @apiParam {String} [address] Regular Expression to match with the address
 * @apiParam {String} [near] Coordinates to do a search by proximity. The String must be longitude,latitude
 * @apiParam {String} [min_distance] If the near parameter is used, it expresses the minimum distance to the coordinates provided
 * @apiParam {String} [max_distance] If the near parameter is used, it expresses the maximum distance to the coordinates provided
 * @apiParamExample {String} Near Syntax example
 *  near=lng,lat&min_distance=500&max_distance=3000
 * @apiParamExample {String} Near Data Example 
 *  near=-3.368520,38.008090&min_distance=500&max_distance=3000
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of properties.
 * @apiSuccess {Object[]} rows List of properties.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/auth',
  token({ required: true }),
  query(eventosSchema),
  authenticatedIndex)

/**
 * @api {get} /properties/mine Retrieve properties of a user
 * @apiName RetrieveProperties of a user
 * @apiGroup Property
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam {String} access_token master access token.
 * @apiParam {Number} [rooms] Number of rooms. It can be a list of values (optional)
 * @apiParamExample {Number} Rooms param example
 * rooms=1,2,3
 * @apiParam {String} [city] Name of the city (optional)
 * @apiParam {String} [province] Name of the province (optional)
 * @apiParam {String} [zipcode] Value of the zipcode (optional)
 * @apiParam {Number} [min_size] Min size of the property (optional)
 * @apiParam {Number} [max_size] Max size of the property (optional)
 * @apiParam {Number} [min_price] Min price of the property (optional)
 * @apiParam {Number} [max_price] Max price of the property (optional)
 * @apiParam {String} [category] Category Id of the property
 * @apiParam {String} [address] Regular Expression to match with the address
 * @apiParam {String} [near] Coordinates to do a search by proximity. The String must be longitude,latitude
 * @apiParam {String} [min_distance] If the near parameter is used, it expresses the minimum distance to the coordinates provided
 * @apiParam {String} [max_distance] If the near parameter is used, it expresses the maximum distance to the coordinates provided
 * @apiParamExample {String} Near Syntax example
 *  near=lng,lat&min_distance=500&max_distance=3000
 * @apiParamExample {String} Near Data Example 
 *  near=-3.368520,38.008090&min_distance=500&max_distance=3000
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of properties.
 * @apiSuccess {Object[]} rows List of properties.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/mine',
  token({ required: true }),
  query(eventosSchema),
  userEventos)

/**
 * @api {get} /properties/fav Retrieve the favorite properties of a user
 * @apiName RetrieveFavsProperties of a user
 * @apiGroup Property
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam {String} access_token master access token.
 * @apiParam {Number} [rooms] Number of rooms. It can be a list of values (optional)
 * @apiParamExample {Number} Rooms param example
 * rooms=1,2,3
 * @apiParam {String} [city] Name of the city (optional)
 * @apiParam {String} [province] Name of the province (optional)
 * @apiParam {String} [zipcode] Value of the zipcode (optional)
 * @apiParam {Number} [min_size] Min size of the property (optional)
 * @apiParam {Number} [max_size] Max size of the property (optional)
 * @apiParam {Number} [min_price] Min price of the property (optional)
 * @apiParam {Number} [max_price] Max price of the property (optional)
 * @apiParam {String} [category] Category Id of the property
 * @apiParam {String} [address] Regular Expression to match with the address
 * @apiParam {String} [near] Coordinates to do a search by proximity. The String must be longitude,latitude
 * @apiParam {String} [min_distance] If the near parameter is used, it expresses the minimum distance to the coordinates provided
 * @apiParam {String} [max_distance] If the near parameter is used, it expresses the maximum distance to the coordinates provided
 * @apiParamExample {String} Near Syntax example
 *  near=lng,lat&min_distance=500&max_distance=3000
 * @apiParamExample {String} Near Data Example 
 *  near=-3.368520,38.008090&min_distance=500&max_distance=3000
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of properties.
 * @apiSuccess {Object[]} rows List of properties.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/fav',
  token({ required: true }),
  query(eventosSchema),
  userFavorites)


/**
 * @api {get} /eventos/:id Retrieve evento
 * @apiName RetrieveEvento
 * @apiGroup Evento
 * @apiSuccess {Object} evento Evento's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Evento not found.
 */
router.get('/:id',
  master(),
  show)

/**
 * @api {put} /eventos/:id Update evento
 * @apiName UpdateEvento
 * @apiGroup Evento
 * @apiParam userId Evento's userId.
 * @apiParam titulo Evento's titulo.
 * @apiParam descripcion Evento's descripcion.
 * @apiParam likes Evento's likes.
 * @apiParam categoriaId Evento's categoriaId.
 * @apiParam direccion Evento's direccion.
 * @apiParam provincia Evento's provincia.
 * @apiParam loc Evento's loc.
 * @apiParam imagenes Evento's imagenes.
 * @apiParam valoracionMedia Evento's valoracionMedia.
 * @apiParam comentarios Evento's comentarios.
 * @apiSuccess {Object} evento Evento's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Evento not found.
 */
router.put('/:id',
  token({required: true}),
  body({ titulo, descripcion, likes, categoriaId, direccion, provincia, loc }),
  update)

/**
 * @api {delete} /eventos/:id Delete evento
 * @apiName DeleteEvento
 * @apiGroup Evento
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Evento not found.
 */
router.delete('/:id',
  token({required: true}),
  destroy)

/**
 * @api {post} /eventos/fav/:id Add a property as favorite
 * @apiName AddFavProperty
 * @apiGroup Property
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} user Users's data.
 * @apiError 401 user access only.
 */
router.post('/fav/:id',
  token({required: true}),
  addFavorite)

/**
 * @api {delete} /eventos/fav/:id Delete a property as a favorite
 * @apiName DeleteFavProperty
 * @apiGroup Property
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 401 user access only.
 */
router.delete('/fav/:id',
  token({required: true}),
  delFavorite)

export default router
