import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Evento } from '.'

const app = () => express(apiRoot, routes)

let evento

beforeEach(async () => {
  evento = await Evento.create({})
})

test('POST /eventos 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ userId: 'test', titulo: 'test', descripcion: 'test', likes: 'test', categoriaId: 'test', direccion: 'test', provincia: 'test', loc: 'test', imagenes: 'test', valoracionMedia: 'test', comentarios: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.userId).toEqual('test')
  expect(body.titulo).toEqual('test')
  expect(body.descripcion).toEqual('test')
  expect(body.likes).toEqual('test')
  expect(body.categoriaId).toEqual('test')
  expect(body.direccion).toEqual('test')
  expect(body.provincia).toEqual('test')
  expect(body.loc).toEqual('test')
  expect(body.imagenes).toEqual('test')
  expect(body.valoracionMedia).toEqual('test')
  expect(body.comentarios).toEqual('test')
})

test('GET /eventos 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /eventos/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${evento.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(evento.id)
})

test('GET /eventos/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /eventos/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${evento.id}`)
    .send({ userId: 'test', titulo: 'test', descripcion: 'test', likes: 'test', categoriaId: 'test', direccion: 'test', provincia: 'test', loc: 'test', imagenes: 'test', valoracionMedia: 'test', comentarios: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(evento.id)
  expect(body.userId).toEqual('test')
  expect(body.titulo).toEqual('test')
  expect(body.descripcion).toEqual('test')
  expect(body.likes).toEqual('test')
  expect(body.categoriaId).toEqual('test')
  expect(body.direccion).toEqual('test')
  expect(body.provincia).toEqual('test')
  expect(body.loc).toEqual('test')
  expect(body.imagenes).toEqual('test')
  expect(body.valoracionMedia).toEqual('test')
  expect(body.comentarios).toEqual('test')
})

test('PUT /eventos/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ userId: 'test', titulo: 'test', descripcion: 'test', likes: 'test', categoriaId: 'test', direccion: 'test', provincia: 'test', loc: 'test', imagenes: 'test', valoracionMedia: 'test', comentarios: 'test' })
  expect(status).toBe(404)
})

test('DELETE /eventos/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${evento.id}`)
  expect(status).toBe(204)
})

test('DELETE /eventos/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
