import { Evento } from '.'

let evento

beforeEach(async () => {
  evento = await Evento.create({ userId: 'test', titulo: 'test', descripcion: 'test', likes: 'test', categoriaId: 'test', direccion: 'test', provincia: 'test', loc: 'test', imagenes: 'test', valoracionMedia: 'test', comentarios: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = evento.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(evento.id)
    expect(view.userId).toBe(evento.userId)
    expect(view.titulo).toBe(evento.titulo)
    expect(view.descripcion).toBe(evento.descripcion)
    expect(view.likes).toBe(evento.likes)
    expect(view.categoriaId).toBe(evento.categoriaId)
    expect(view.direccion).toBe(evento.direccion)
    expect(view.provincia).toBe(evento.provincia)
    expect(view.loc).toBe(evento.loc)
    expect(view.imagenes).toBe(evento.imagenes)
    expect(view.valoracionMedia).toBe(evento.valoracionMedia)
    expect(view.comentarios).toBe(evento.comentarios)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = evento.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(evento.id)
    expect(view.userId).toBe(evento.userId)
    expect(view.titulo).toBe(evento.titulo)
    expect(view.descripcion).toBe(evento.descripcion)
    expect(view.likes).toBe(evento.likes)
    expect(view.categoriaId).toBe(evento.categoriaId)
    expect(view.direccion).toBe(evento.direccion)
    expect(view.provincia).toBe(evento.provincia)
    expect(view.loc).toBe(evento.loc)
    expect(view.imagenes).toBe(evento.imagenes)
    expect(view.valoracionMedia).toBe(evento.valoracionMedia)
    expect(view.comentarios).toBe(evento.comentarios)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
