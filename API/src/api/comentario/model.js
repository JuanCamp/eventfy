import mongoose, { Schema } from 'mongoose'

const comentarioSchema = new Schema({
  autor: {
    type: String
  },
  nombreAutor: {
    type: String
  },
  contenido: {
    type: String
  },
  valoracion: {
    type: String
  },
  imagenAutor: {
    type: String
  },
  evento: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

comentarioSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      autor: this.autor,
      nombreAutor: this.nombreAutor,
      contenido: this.contenido,
      valoracion: this.valoracion,
      imagenAutor: this.imagenAutor,
      evento: this.evento,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Comentario', comentarioSchema)

export const schema = model.schema
export default model
