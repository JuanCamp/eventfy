import { Comentario } from '.'

let comentario

beforeEach(async () => {
  comentario = await Comentario.create({ autor: 'test', nombreAutor: 'test', contenido: 'test', valoracion: 'test', imagenAutor: 'test', evento: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = comentario.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(comentario.id)
    expect(view.autor).toBe(comentario.autor)
    expect(view.nombreAutor).toBe(comentario.nombreAutor)
    expect(view.contenido).toBe(comentario.contenido)
    expect(view.valoracion).toBe(comentario.valoracion)
    expect(view.imagenAutor).toBe(comentario.imagenAutor)
    expect(view.evento).toBe(comentario.evento)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = comentario.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(comentario.id)
    expect(view.autor).toBe(comentario.autor)
    expect(view.nombreAutor).toBe(comentario.nombreAutor)
    expect(view.contenido).toBe(comentario.contenido)
    expect(view.valoracion).toBe(comentario.valoracion)
    expect(view.imagenAutor).toBe(comentario.imagenAutor)
    expect(view.evento).toBe(comentario.evento)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
