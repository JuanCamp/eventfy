package com.example.eventfyapp.ViewModels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.eventfyapp.Model.Evento;

public class SubirFotoViewModel extends ViewModel {

    private final MutableLiveData<Evento> eventoMutableLiveData = new MutableLiveData<>();

    public void selectedAplicar(Evento evento){
        eventoMutableLiveData.setValue(evento);
    }

    public MutableLiveData<Evento> getAll(){
        return eventoMutableLiveData;
    }
}
