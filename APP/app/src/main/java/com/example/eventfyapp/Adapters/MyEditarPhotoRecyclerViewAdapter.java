package com.example.eventfyapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.eventfyapp.Generator.ServiceGenerator;
import com.example.eventfyapp.Generator.TipoAutenticacion;
import com.example.eventfyapp.Generator.UtilToken;
import com.example.eventfyapp.Listener.EditarPhotosListener;
import com.example.eventfyapp.Model.CrearEventoDto;
import com.example.eventfyapp.Model.Evento;
import com.example.eventfyapp.R;
import com.example.eventfyapp.Responses.CrearEventoResponse;
import com.example.eventfyapp.Services.EventoService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyEditarPhotoRecyclerViewAdapter extends RecyclerView.Adapter<MyEditarPhotoRecyclerViewAdapter.ViewHolder> {

    private final Evento mValues;
    List<String> photos;
    private final EditarPhotosListener mListener;
    private Context context;

    public MyEditarPhotoRecyclerViewAdapter(Context cxt, Evento items, EditarPhotosListener listener) {
        context = cxt;
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_editarphoto, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues;
        photos = new ArrayList<String>(Arrays.asList(mValues.getFotos()));

            holder.imageView_photo.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide
                    .with(context)
                    .load(mValues.getFotos()[position])
                    .into(holder.imageView_photo);

            holder.imageView_delete_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ( eliminarPhoto(mValues.getFotos()[position]) != -1){
                        int posicion = eliminarPhoto(mValues.getFotos()[position]);

                        photos.remove(posicion);

                        String[] imagenesActualizadas = photos.toArray(new String[0]);

                        mValues.setFotos(imagenesActualizadas);

                        actualizarPropiedad();


                    }

                }
            });



    }

    private void actualizarPropiedad() {
        CrearEventoDto crearEventoDto = new CrearEventoDto(
                mValues.getTitulo(),
                mValues.getDescripcion(),
                mValues.getDireccion(),
                mValues.getProvincia(),
                mValues.getLoc(),
                mValues.getFotos()

        );
        EventoService service = ServiceGenerator.createService(EventoService.class, UtilToken.getToken(context), TipoAutenticacion.JWT);
        Call<CrearEventoResponse> call = service.editEvento(mValues.getId(), crearEventoDto);

        call.enqueue(new Callback<CrearEventoResponse>() {
            @Override
            public void onResponse(Call<CrearEventoResponse> call, Response<CrearEventoResponse> response) {

            }

            @Override
            public void onFailure(Call<CrearEventoResponse> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private int eliminarPhoto(String photo) {
        boolean encontrado = false;
        int i = 0;

        while (i < photos.size() && !encontrado) {
            if (photo == photos.get(i)) {
                encontrado = true;
            } else {
                i++;
            }
        }

        if (encontrado) {
            return i;
        } else {
            return -1;
        }

    }

    @Override
    public int getItemCount() {
        return mValues.getFotos().length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        //public final TextView mIdView;
        public final TextView mContentView;
        public Evento mItem;
        public final ImageView imageView_photo, imageView_delete_photo;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            //mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
            imageView_photo = view.findViewById(R.id.imageView_photo);
            imageView_delete_photo = view.findViewById(R.id.imageView_delete_photo);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
