package com.example.eventfyapp.Services;

import com.example.eventfyapp.Model.Foto;
import com.example.eventfyapp.Responses.ResponseContainer;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface PhotoService {
    @GET("/fotos")
    Call<ResponseContainer<Foto>> getPhotoOfEvento(@Query("eventoId") String eventoId);

    @Multipart
    @POST("/fotos")
    Call<Foto> addPhoto(@Part MultipartBody.Part photo,
                         @Part("eventoId") RequestBody eventoId);
}
