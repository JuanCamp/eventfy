package com.example.eventfyapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventfyapp.Adapters.ImageAdapter;
import com.example.eventfyapp.Generator.ServiceGenerator;
import com.example.eventfyapp.Generator.TipoAutenticacion;
import com.example.eventfyapp.Generator.UtilToken;
import com.example.eventfyapp.Generator.UtilUser;
import com.example.eventfyapp.Model.Evento;
import com.example.eventfyapp.Responses.ResponseContainerNoList;
import com.example.eventfyapp.Services.EventoService;
import com.google.android.gms.maps.MapView;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventoDetallado extends AppCompatActivity {

    ViewPager viewPager;
    TextView textView_direccion_detalle, textView_province_detalle, textView_descripcion, textView_categoria;
    MapView mapView;
    Evento evento;
    private List<String> imagenes;
    private MenuItem editPhotos;
    private String id;
    private Menu menu;
    private MenuItem action_editarPhoto, action_editarEvento, action_eliminarEvento;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento_detallado);

        findsId();

    }


    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();

        id = intent.getStringExtra("id");

        EventoService eventoService = ServiceGenerator.createService(EventoService.class);

        Call<ResponseContainerNoList<Evento>> call = eventoService.getOneEvento(id);

        call.enqueue(new Callback<ResponseContainerNoList<Evento>>() {
            @Override
            public void onResponse(Call<ResponseContainerNoList<Evento>> call, Response<ResponseContainerNoList<Evento>> response) {
                if (response.code() != 200) {
                    Toast.makeText(EventoDetallado.this, "Error al ver Evento", Toast.LENGTH_SHORT).show();
                } else {
                    evento = response.body().getRows();
                    if (UtilUser.getNombre(EventoDetallado.this) != null) {
                        if (UtilUser.getNombre(EventoDetallado.this).equals(evento.getOwnerIdname())) {

                        } else {
                            getSupportActionBar().hide();
                        }
                    } else {
                        getSupportActionBar().hide();
                    }

                    sets();
                }
            }

            @Override
            public void onFailure(Call<ResponseContainerNoList<Evento>> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(EventoDetallado.this, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_editarPhoto:
                Intent i = new Intent(EventoDetallado.this, EditarPhoto.class);
                i.putExtra("id", evento.getId());
                startActivity(i);
                return true;
            case R.id.action_editarEvento:
                startActivity(new Intent(EventoDetallado.this, AddEvento.class).putExtra("idevento", evento.getId()));
                return true;
            case R.id.action_eliminarEvento:
                dialogoDeEliminar();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void dialogoDeEliminar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("¿Estás seguro?")
                .setTitle("¿Quieres eliminar el evento?");
        builder.setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                eliminarEvento();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void eliminarEvento() {
        EventoService service = ServiceGenerator.createService(EventoService.class, UtilToken.getToken(this), TipoAutenticacion.JWT);
        Call<ResponseContainerNoList<Evento>> call = service.deleteEvento(id);

        call.enqueue(new Callback<ResponseContainerNoList<Evento>>() {
            @Override
            public void onResponse(Call<ResponseContainerNoList<Evento>> call, Response<ResponseContainerNoList<Evento>> response) {
                if (response.code() != 204) {
                    Toast.makeText(EventoDetallado.this, "Fallo al borrar", Toast.LENGTH_SHORT).show();
                } else {
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseContainerNoList<Evento>> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(EventoDetallado.this, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sets() {
        textView_descripcion.setText(evento.getDescripcion());
        textView_province_detalle.setText(evento.getProvincia());
        textView_direccion_detalle.setText(evento.getDireccion());
        textView_categoria.setText(evento.getCategoryId());


        imagenes = Arrays.asList(evento.getFotos());

        ImageAdapter imageAdapter = new ImageAdapter(EventoDetallado.this, imagenes);

/*        if (UtilUser.getId(this) != null) {
            if (!UtilUser.getId(this).equals(propiedad.getOwnerId())) {
                floatingActionButtonEditPhoto.hide();
            }
        }*/

        viewPager.setAdapter(imageAdapter);
    }

    private void findsId() {

        viewPager = findViewById(R.id.viewPager);
        textView_direccion_detalle = findViewById(R.id.textView_direccion_detalle);
        textView_province_detalle = findViewById(R.id.textView_province_detalle);
        textView_descripcion = findViewById(R.id.textView_descripcion);
        textView_categoria = findViewById(R.id.textView_categoria);
        editPhotos = findViewById(R.id.action_editarPhoto);
        action_eliminarEvento = findViewById(R.id.action_eliminarEvento);
        action_editarPhoto = findViewById(R.id.action_editarPhoto);
        action_editarEvento = findViewById(R.id.action_editarEvento);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }
}
