package com.example.eventfyapp.Fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventfyapp.Adapters.MyEditarPhotoRecyclerViewAdapter;
import com.example.eventfyapp.EditarPhoto;
import com.example.eventfyapp.Generator.ServiceGenerator;
import com.example.eventfyapp.Listener.EditarPhotosListener;
import com.example.eventfyapp.Model.Evento;
import com.example.eventfyapp.R;
import com.example.eventfyapp.Responses.ResponseContainerNoList;
import com.example.eventfyapp.Services.EventoService;
import com.example.eventfyapp.ViewModels.SubirFotoViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class editarPhotoFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 2    ;
    private EditarPhotosListener mListener;
    private Evento fotos;
    private MyEditarPhotoRecyclerViewAdapter adapter;
    private Context cxt;


    public editarPhotoFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static editarPhotoFragment newInstance(int columnCount) {
        editarPhotoFragment fragment = new editarPhotoFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_editarphoto_list, container, false);

        EditarPhoto activity = (EditarPhoto) getActivity();
        String id = activity.getIdEvento();

        // Set the adapter
        if (view instanceof RecyclerView) {
            cxt = view.getContext();
            final RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(cxt));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(cxt, mColumnCount));
            }

            final EventoService service = ServiceGenerator.createService(EventoService.class);
            Call<ResponseContainerNoList<Evento>> call = service.getOneEvento(id);

            call.enqueue(new Callback<ResponseContainerNoList<Evento>>() {
                @Override
                public void onResponse(Call<ResponseContainerNoList<Evento>> call, Response<ResponseContainerNoList<Evento>> response) {
                    if (response.code() != 200) {
                        Toast.makeText(getContext(), "Fallo al traer eventos", Toast.LENGTH_SHORT).show();
                    } else {
                        fotos = response.body().getRows();

                        adapter = new MyEditarPhotoRecyclerViewAdapter(
                                cxt,
                                fotos,
                                mListener
                        );

                        recyclerView.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<ResponseContainerNoList<Evento>> call, Throwable t) {
                    Log.e("NetworkFailure", t.getMessage());
                    Toast.makeText(getContext(), "Error de conexión", Toast.LENGTH_SHORT).show();
                }
            });


            SubirFotoViewModel subirFotoViewModel = ViewModelProviders.of(getActivity()).get(SubirFotoViewModel.class);

            subirFotoViewModel.getAll().observe(getActivity(), new Observer<Evento>() {
                @Override
                public void onChanged(@Nullable Evento evento) {
                    EditarPhoto activity = (EditarPhoto) getActivity();
                    String id = activity.getIdEvento();
                    final EventoService service = ServiceGenerator.createService(EventoService.class);
                    Call<ResponseContainerNoList<Evento>> call = service.getOneEvento(id);

                    call.enqueue(new Callback<ResponseContainerNoList<Evento>>() {
                        @Override
                        public void onResponse(Call<ResponseContainerNoList<Evento>> call, Response<ResponseContainerNoList<Evento>> response) {
                            if (response.code() != 200) {
                                Toast.makeText(getContext(), "Fallo al obtener eventos", Toast.LENGTH_SHORT).show();
                            } else {
                                fotos = response.body().getRows();

                                adapter = new MyEditarPhotoRecyclerViewAdapter(
                                        cxt,
                                        fotos,
                                        mListener
                                );

                                recyclerView.setAdapter(adapter);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseContainerNoList<Evento>> call, Throwable t) {
                            Log.e("NetworkFailure", t.getMessage());
                            Toast.makeText(getContext(), "Error de conexión", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });


        }


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EditarPhotosListener) {
            mListener = (EditarPhotosListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement EditarPhotosListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
