package com.example.eventfyapp.Model;

public class Foto {
    public String id;
    public String eventoId;
    public String imgurLink;
    public String deletehash;


    public Foto(String id, String eventoId, String imgurLink, String deletehash) {
        this.id = id;
        this.eventoId = eventoId;
        this.imgurLink = imgurLink;
        this.deletehash = deletehash;
    }

    public Foto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventoId() {
        return eventoId;
    }

    public void setEventoId(String eventoId) {
        this.eventoId = eventoId;
    }

    public String getImgurLink() {
        return imgurLink;
    }

    public void setImgurLink(String imgurLink) {
        this.imgurLink = imgurLink;
    }

    public String getDeletehash() {
        return deletehash;
    }

    public void setDeletehash(String deletehash) {
        this.deletehash = deletehash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Foto foto = (Foto) o;

        if (id != null ? !id.equals(foto.id) : foto.id != null) return false;
        if (eventoId != null ? !eventoId.equals(foto.eventoId) : foto.eventoId != null)
            return false;
        if (imgurLink != null ? !imgurLink.equals(foto.imgurLink) : foto.imgurLink != null)
            return false;
        return deletehash != null ? deletehash.equals(foto.deletehash) : foto.deletehash == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (eventoId != null ? eventoId.hashCode() : 0);
        result = 31 * result + (imgurLink != null ? imgurLink.hashCode() : 0);
        result = 31 * result + (deletehash != null ? deletehash.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "id='" + id + '\'' +
                ", propertyId='" + eventoId + '\'' +
                ", imgurLink='" + imgurLink + '\'' +
                ", deletehash='" + deletehash + '\'' +
                '}';
    }
}
