package com.example.eventfyapp.Model;

public class EventoFavDto {

    private String titulo;
    private String descripcion;
    private int like;
    private String categoryId;
    private String direccion;
    private String provincia;
    private String loc;
    private String [] imagenes;
    private double valoracionMedia;
    public String id;
    public String [] fotos= null;

    public EventoFavDto() {
    }

    public EventoFavDto(String titulo, String descripcion, int like, String categoryId, String direccion, String provincia, String loc, String[] imagenes, double valoracionMedia, String id, String[] fotos) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.like = like;
        this.categoryId = categoryId;
        this.direccion = direccion;
        this.provincia = provincia;
        this.loc = loc;
        this.imagenes = imagenes;
        this.valoracionMedia = valoracionMedia;
        this.id = id;
        this.fotos = fotos;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getLike() {
        return like;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getLoc() {
        return loc;
    }

    public String[] getImagenes() {
        return imagenes;
    }

    public double getValoracionMedia() {
        return valoracionMedia;
    }

    public String getId() {
        return id;
    }

    public String[] getFotos() {
        return fotos;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public void setImagenes(String[] imagenes) {
        this.imagenes = imagenes;
    }

    public void setValoracionMedia(double valoracionMedia) {
        this.valoracionMedia = valoracionMedia;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFotos(String[] fotos) {
        this.fotos = fotos;
    }
}
