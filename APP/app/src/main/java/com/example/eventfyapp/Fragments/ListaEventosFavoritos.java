package com.example.eventfyapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventfyapp.Adapters.MyEventoRecyclerViewAdapter;
import com.example.eventfyapp.Generator.ServiceGenerator;
import com.example.eventfyapp.Generator.TipoAutenticacion;
import com.example.eventfyapp.Generator.UtilToken;
import com.example.eventfyapp.Listener.EventoListener;
import com.example.eventfyapp.Model.Evento;
import com.example.eventfyapp.R;
import com.example.eventfyapp.Responses.ResponseContainer;
import com.example.eventfyapp.Services.EventoService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListaEventosFavoritos extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private EventoListener mListener;
    private List<Evento> listaEventosFavoritos;
    private MyEventoRecyclerViewAdapter adapter;
    private Context cxt;

    public ListaEventosFavoritos() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ListaEventosFavoritos newInstance(int columnCount) {
        ListaEventosFavoritos fragment = new ListaEventosFavoritos();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_evento_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            cxt = view.getContext();
            final RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(cxt));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(cxt, mColumnCount));
            }


            listaEventosFavoritos = new ArrayList<>();


            llamadaALaLista(recyclerView);


        }
        return view;
    }

    private void llamadaALaLista(final RecyclerView recyclerView) {
        EventoService service = ServiceGenerator.createService(EventoService.class, UtilToken.getToken(getContext()), TipoAutenticacion.JWT);
        Call<ResponseContainer<Evento>> call = service.getFavEventos();

        call.enqueue(new Callback<ResponseContainer<Evento>>() {
            @Override
            public void onResponse(Call<ResponseContainer<Evento>> call, Response<ResponseContainer<Evento>> response) {
                if (response.code() != 200) {
                    Toast.makeText(getActivity(), "Error en petición", Toast.LENGTH_SHORT).show();
                } else {
                    listaEventosFavoritos = response.body().getRows();

                    adapter = new MyEventoRecyclerViewAdapter(
                            cxt,
                            listaEventosFavoritos,
                            mListener,
                            true
                    );
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseContainer<Evento>> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(getActivity(), "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EventoListener) {
            mListener = (EventoListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement EventoListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
