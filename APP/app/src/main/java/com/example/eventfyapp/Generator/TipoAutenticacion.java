package com.example.eventfyapp.Generator;

public enum TipoAutenticacion {
    SIN_AUTENTICACION, BASIC, JWT, SIN_MASTER
}
