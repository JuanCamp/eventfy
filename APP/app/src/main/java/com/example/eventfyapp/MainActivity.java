package com.example.eventfyapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.eventfyapp.Fragments.EventoFragment;
import com.example.eventfyapp.Fragments.ListaEventosDelUsuarioFragment;
import com.example.eventfyapp.Fragments.ListaEventosFavoritos;
import com.example.eventfyapp.Fragments.ProfileFragment;
import com.example.eventfyapp.Generator.UtilUser;
import com.example.eventfyapp.Listener.EventoListener;
import com.example.eventfyapp.Listener.PropertyListener;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements EventoListener {

    private FrameLayout contenedor;
    private ImageView imageView_fav;
    private Menu menu;
    private FloatingActionButton fab;
    private boolean filtro;
    private HashMap<String, String> data;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_inicio:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.contenedor_main, new EventoFragment())
                            .commit();
                    enseniarFab();
                    return true;
                case R.id.navigation_favoritos:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.contenedor_main, new ListaEventosFavoritos())
                            .commit();
                    enseniarFab();
                    return true;
                case R.id.navigation_misEventos:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.contenedor_main, new ListaEventosDelUsuarioFragment())
                            .commit();
                    enseniarFab();
                    return true;
                case R.id.navigation_perfil:
                    if (UtilUser.getEmail(MainActivity.this) == null) {
                        Intent i = new Intent(MainActivity.this, SessionActivity.class);
                        startActivity(i);
                    } else {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.contenedor_main, new ProfileFragment())
                                .commit();
                        fab.hide();
                    }

                    return true;

            }
            return false;
        }
    };

    private void enseniarFab() {
        if(UtilUser.getNombre(this) != null) {
            fab.show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        menu = navigation.getMenu();

        contenedor = findViewById(R.id.contenedor_main);
        fab = findViewById(R.id.fab);

        if(getIntent().getExtras() != null){
            data = (HashMap<String,String>) getIntent().getExtras().get("data");
        }

        events();

    }

    public HashMap<String, String> getData() {
        return data;
    }

    private void events() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AddEvento.class);
                startActivity(i);
            }
        });
    }

    public boolean isFiltro() {
        return filtro;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        switch (id) {
            case R.id.action_map:
                Intent i = new Intent(MainActivity.this, Mapa.class);
                startActivity(i);
                return true;
            case R.id.filter_list:

                Intent in = new Intent(MainActivity.this, Filtros.class);
                startActivity(in);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void verEvento(String casa) {

    }

    public void addFavEvento(String id) {
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contenedor_main, new EventoFragment())
                .commit();

        ocultarBotonesParaAnonimos(menu);


        MenuItem inicio = menu.findItem(R.id.navigation_inicio);
        inicio.setChecked(true);
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("¿Estás seguro?")
                .setTitle("¿Quieres cerrar sesión?");

        builder.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                UtilUser.clearSharedPreferences(MainActivity.this);
                finish();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    public void ocultarBotonesParaAnonimos(Menu menu) {
        if (UtilUser.getEmail(this) == null) {
            MenuItem item = menu.findItem(R.id.navigation_favoritos);
            item.setVisible(false);
            MenuItem item2 = menu.findItem(R.id.navigation_misEventos);
            item2.setVisible(false);

            fab.hide();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar2, menu);
        return true;
    }

}
