package com.example.eventfyapp.Model;

public class Evento {

    private String titulo;
    private String descripcion;
    private int like;
    private String categoryId;
    private String direccion;
    private String provincia;
    private String loc;
    private String [] imagenes;
    private double valoracionMedia;
    public String createdAt;
    public String updatedAt;
    public Integer v;
    public String id;
    public String [] fotos= null;
    public boolean isFav;
    public UserId userId;


    public Evento() {
    }

    public Evento(String titulo, String descripcion, int like, String categoryId, String direccion, String provincia, String loc, String[] imagenes, double valoracionMedia) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.like = like;
        this.categoryId = categoryId;
        this.direccion = direccion;
        this.provincia = provincia;
        this.loc = loc;
        this.imagenes = imagenes;
        this.valoracionMedia = valoracionMedia;
    }

    public Evento(String titulo, String descripcion, int like, String categoryId, String direccion, String provincia, String loc, String[] imagenes, double valoracionMedia, String createdAt, String updatedAt, Integer v, String id, String[] fotos, boolean isFav) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.like = like;
        this.categoryId = categoryId;
        this.direccion = direccion;
        this.provincia = provincia;
        this.loc = loc;
        this.imagenes = imagenes;
        this.valoracionMedia = valoracionMedia;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.v = v;
        this.id = id;
        this.fotos = fotos;
        this.isFav = isFav;
    }

    public Evento(String titulo, String descripcion, int like, String categoryId, String direccion, String provincia, String loc, String[] imagenes, double valoracionMedia, String createdAt, String updatedAt, Integer v, String id, String[] fotos, boolean isFav, UserId userId) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.like = like;
        this.categoryId = categoryId;
        this.direccion = direccion;
        this.provincia = provincia;
        this.loc = loc;
        this.imagenes = imagenes;
        this.valoracionMedia = valoracionMedia;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.v = v;
        this.id = id;
        this.fotos = fotos;
        this.isFav = isFav;
        this.userId = userId;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getLike() {
        return like;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getLoc() {
        return loc;
    }

    public String[] getImagenes() {
        return imagenes;
    }

    public double getValoracionMedia() {
        return valoracionMedia;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public String getId() {
        return id;
    }

    public String[] getFotos() {
        return fotos;
    }

    public boolean isFav() {
        return isFav;
    }

    public UserId getOwnerId() {
        return userId;
    }

    public String getOwnerIdname() {

        return  userId.getName();
    }

    public String getUserIdname() {
        return  userId.getName();
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public void setImagenes(String[] imagenes) {
        this.imagenes = imagenes;
    }

    public void setValoracionMedia(double valoracionMedia) {
        this.valoracionMedia = valoracionMedia;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFotos(String[] fotos) {
        this.fotos = fotos;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }

    public void setOwnerId(UserId ownerId) {
        this.userId = ownerId;
    }
}

class UserId {
    private String id;
    private String picture;
    private String name;

    public UserId(String id, String picture, String name) {
        this.id = id;
        this.picture = picture;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}