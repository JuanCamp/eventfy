package com.example.eventfyapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.eventfyapp.EventoDetallado;
import com.example.eventfyapp.Generator.ServiceGenerator;
import com.example.eventfyapp.Generator.TipoAutenticacion;
import com.example.eventfyapp.Generator.UtilToken;
import com.example.eventfyapp.Generator.UtilUser;
import com.example.eventfyapp.Listener.EventoListener;
import com.example.eventfyapp.Model.Evento;
import com.example.eventfyapp.Model.EventoFavDto;
import com.example.eventfyapp.Model.Foto;
import com.example.eventfyapp.Model.addFavouriteDto;
import com.example.eventfyapp.R;
import com.example.eventfyapp.Services.EventoService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyEventoRecyclerViewAdapter extends RecyclerView.Adapter<MyEventoRecyclerViewAdapter.ViewHolder> {

    private List<Evento> mValues = null;
    private EventoListener mListener;
    private Context contexto;
    private Foto foto;
    private List<EventoFavDto> valores = null;
    private boolean favorito;
    private boolean fragmentoFav;


    public MyEventoRecyclerViewAdapter(Context cxt, EventoListener listener, List<EventoFavDto> objetos) {
        contexto = cxt;
        valores = objetos;
        mListener = listener;
    }

    public MyEventoRecyclerViewAdapter(Context cxt, List<Evento> items, EventoListener listener) {
        contexto = cxt;
        mValues = items;
        mListener = listener;
    }

    public MyEventoRecyclerViewAdapter(Context cxt, List<Evento> items, EventoListener listener, boolean fragmentoDeFavoritos) {
        contexto = cxt;
        mValues = items;
        mListener = listener;
        fragmentoFav = fragmentoDeFavoritos;
    }

    public void setListaFiltrada(List<Evento> listaFiltrada) {
        this.mValues = listaFiltrada;
        notifyDataSetChanged();
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_evento, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (mValues != null) {
            holder.mItem = mValues.get(position);
            eventsNormal(holder, position);
        } else {
            holder.item = valores.get(position);
            eventsMine(holder, position);
        }

    }

    private void eventsMine(final ViewHolder holder, final int position) {

        holder.textView_title.setText(valores.get(position).getTitulo());
        holder.textView_direccion.setText(valores.get(position).getDireccion());
        holder.textView_ciudad.setText(valores.get(position).getProvincia());
        holder.imageView_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EventoService service = ServiceGenerator.createService(EventoService.class, UtilToken.getToken(contexto), TipoAutenticacion.JWT);
                Call<addFavouriteDto> call = service.addFavEvento(holder.item.getId());

                call.enqueue(new Callback<addFavouriteDto>() {
                    @Override
                    public void onResponse(Call<addFavouriteDto> call, Response<addFavouriteDto> response) {
                        if (response.code() == 200) {
                            holder.imageView_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
                        } else {
                            Toast.makeText(contexto, "Error en petición", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<addFavouriteDto> call, Throwable t) {
                        Log.e("NetworkFailure", t.getMessage());
                        Toast.makeText(contexto, "Error de conexión", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(contexto, EventoDetallado.class);
                i.putExtra("id", valores.get(position).getId());
                contexto.startActivity(i);
            }
        });

        holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        if(valores.get(position).getFotos() != null) {
            Glide
                    .with(contexto)
                    .load(valores.get(position).getFotos()[0])
                    .into(holder.imageView);
        }


    }

    private void eventsNormal(final ViewHolder holder, final int position) {

        holder.textView_title.setText(mValues.get(position).getTitulo());
        holder.textView_direccion.setText(mValues.get(position).getDireccion());
        holder.textView_ciudad.setText(mValues.get(position).getProvincia());

        if (UtilUser.getEmail(contexto) == null) {
            holder.imageView_fav.setVisibility(View.GONE);
        } else {
            favorito = mValues.get(position).isFav();

            if (favorito || fragmentoFav) {
                holder.imageView_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
            } else {
                holder.imageView_fav.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            }

            holder.imageView_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    favorito = mValues.get(position).isFav();
                    if (favorito || fragmentoFav) {
                        eventoOnClickCuandoEsFav(holder);
                        mValues.get(position).setFav(false);
                    } else {
                        eventoOnClickCuandoNoEsFav(holder);
                        mValues.get(position).setFav(true);
                    }
                }
            });


        }


        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(contexto, EventoDetallado.class);
                i.putExtra("id", mValues.get(position).getId());
                contexto.startActivity(i);
            }
        });

        holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        if(mValues.get(position).getFotos() != null) {
            Glide
                    .with(contexto)
                    .load(mValues.get(position).getFotos()[0])
                    .into(holder.imageView);
        }
    }

    private void eventoOnClickCuandoEsFav(final ViewHolder holder) {
        EventoService service = ServiceGenerator.createService(EventoService.class, UtilToken.getToken(contexto), TipoAutenticacion.JWT);
        Call<addFavouriteDto> call = service.removeFavEvento(holder.mItem.getId());

        call.enqueue(new Callback<addFavouriteDto>() {
            @Override
            public void onResponse(Call<addFavouriteDto> call, Response<addFavouriteDto> response) {
                if (response.code() == 200) {
                    holder.imageView_fav.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    Toast.makeText(contexto, "Eliminado de favoritos", Toast.LENGTH_SHORT).show();
                    favorito = !favorito;
                } else {
                    Toast.makeText(contexto, "Error en petición", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<addFavouriteDto> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(contexto, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void eventoOnClickCuandoNoEsFav(final ViewHolder holder) {

        EventoService service = ServiceGenerator.createService(EventoService.class, UtilToken.getToken(contexto), TipoAutenticacion.JWT);
        Call<addFavouriteDto> call = service.addFavEvento(holder.mItem.getId());

        call.enqueue(new Callback<addFavouriteDto>() {
            @Override
            public void onResponse(Call<addFavouriteDto> call, Response<addFavouriteDto> response) {
                if (response.code() == 200) {
                    holder.imageView_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
                    Toast.makeText(contexto, "Añadido a favoritos", Toast.LENGTH_SHORT).show();
                    favorito = !favorito;
                } else {
                    Toast.makeText(contexto, "Error en petición", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<addFavouriteDto> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(contexto, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mValues != null) {
            return mValues.size();
        } else {
            return valores.size();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView textView_title, textView_direccion, textView_ciudad;
        public final ImageView imageView_fav, imageView;
        public Evento mItem;
        public EventoFavDto item;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            textView_title = view.findViewById(R.id.textView_titulo);
            textView_direccion = view.findViewById(R.id.textView_address);
            textView_ciudad = view.findViewById(R.id.textView_province);
            imageView = view.findViewById(R.id.imageViewEvento);
            imageView_fav = view.findViewById(R.id.imageView_fav);

        }
    }
}
