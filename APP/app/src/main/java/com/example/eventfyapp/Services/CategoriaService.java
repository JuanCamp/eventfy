package com.example.eventfyapp.Services;

import com.example.eventfyapp.Model.Categoria;
import com.example.eventfyapp.Responses.ResponseContainer;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoriaService {

    @GET("/categorias")
    Call<ResponseContainer<Categoria>> getCategorias();
}
