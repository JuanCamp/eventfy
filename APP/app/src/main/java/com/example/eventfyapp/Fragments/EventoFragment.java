package com.example.eventfyapp.Fragments;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventfyapp.Adapters.MyEventoRecyclerViewAdapter;
import com.example.eventfyapp.Generator.ServiceGenerator;
import com.example.eventfyapp.Generator.TipoAutenticacion;
import com.example.eventfyapp.Generator.UtilToken;
import com.example.eventfyapp.Generator.UtilUser;
import com.example.eventfyapp.Listener.EventoListener;
import com.example.eventfyapp.MainActivity;
import com.example.eventfyapp.Model.Evento;
import com.example.eventfyapp.R;
import com.example.eventfyapp.Responses.ResponseContainer;
import com.example.eventfyapp.Services.EventoService;
import com.example.eventfyapp.ViewModels.FiltroViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventoFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private EventoListener mListener;
    private List<Evento> listaEventos;
    private MyEventoRecyclerViewAdapter adapter;
    private Context cxt;
    private boolean filtrado;

    public EventoFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static EventoFragment newInstance(int columnCount) {
        EventoFragment fragment = new EventoFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_evento_list, container, false);

        if (view instanceof RecyclerView) {
            cxt = view.getContext();
            final RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(cxt));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(cxt, mColumnCount));
            }
            listaEventos = new ArrayList<>();

            if (getActivity().getIntent().getExtras() != null && !filtrado){
                getEventoFiltrado(recyclerView);
                filtrado = !filtrado;
            } else {
                getEventos(recyclerView);
                filtrado = !filtrado;
            }


        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        FiltroViewModel filtroViewModel = ViewModelProviders.of((MainActivity) cxt).get(FiltroViewModel.class);

        if (getActivity() != null) {
            filtroViewModel.getAll().observe(getActivity(), new Observer<List<Evento>>() {
                @Override
                public void onChanged(@Nullable List<Evento> eventos) {
                    adapter = new MyEventoRecyclerViewAdapter(
                            cxt,
                            eventos,
                            mListener
                    );
                }
            });

        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EventoListener) {
            mListener = (EventoListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void getEventos(final RecyclerView recyclerView) {

        if (UtilUser.getEmail(getContext()) == null) {
            llamadaALaListaSinAuth(recyclerView);
        } else {
            llamadaALaListaConAuth(recyclerView);
        }

    }

    public void getEventoFiltrado(final RecyclerView recyclerView){
        HashMap<String, String> data;

        Activity activity = (MainActivity) getActivity();

        data = ((MainActivity) activity).getData();

        EventoService service = ServiceGenerator.createService(EventoService.class);
        Call<ResponseContainer<Evento>> call = service.getEventos(data);

        call.enqueue(new Callback<ResponseContainer<Evento>>() {
            @Override
            public void onResponse(Call<ResponseContainer<Evento>> call, Response<ResponseContainer<Evento>> response) {
                if (response.isSuccessful()){
                    listaEventos = response.body().getRows();

                    adapter = new MyEventoRecyclerViewAdapter(
                            cxt,
                            listaEventos,
                            mListener
                    );
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseContainer<Evento>> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(getContext(), "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void llamadaALaListaConAuth(final RecyclerView recyclerView) {
        EventoService service = ServiceGenerator.createService(EventoService.class, UtilToken.getToken(getContext()), TipoAutenticacion.JWT);
        Call<ResponseContainer<Evento>> call = service.getAuthEventos();

        call.enqueue(new Callback<ResponseContainer<Evento>>() {
            @Override
            public void onResponse(Call<ResponseContainer<Evento>> call, Response<ResponseContainer<Evento>> response) {
                if (response.code() != 200) {
                    Toast.makeText(getActivity(), "Error en petición", Toast.LENGTH_SHORT).show();
                } else {
                    listaEventos = response.body().getRows();

                    adapter = new MyEventoRecyclerViewAdapter(
                            cxt,
                            listaEventos,
                            mListener
                    );
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseContainer<Evento>> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(getActivity(), "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void llamadaALaListaSinAuth(final RecyclerView recyclerView) {
        EventoService service = ServiceGenerator.createService(EventoService.class);
        Call<ResponseContainer<Evento>> call = service.getEventos();

        call.enqueue(new Callback<ResponseContainer<Evento>>() {
            @Override
            public void onResponse(Call<ResponseContainer<Evento>> call, Response<ResponseContainer<Evento>> response) {
                if (response.code() != 200) {
                    Toast.makeText(getActivity(), "Error en petición", Toast.LENGTH_SHORT).show();
                } else {
                    listaEventos = response.body().getRows();

                    adapter = new MyEventoRecyclerViewAdapter(
                            cxt,
                            listaEventos,
                            mListener
                    );
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseContainer<Evento>> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(getActivity(), "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
