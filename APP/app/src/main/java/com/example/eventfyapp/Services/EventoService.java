package com.example.eventfyapp.Services;

import com.example.eventfyapp.Model.CrearEventoDto;
import com.example.eventfyapp.Model.Evento;
import com.example.eventfyapp.Model.EventoFavDto;
import com.example.eventfyapp.Model.addFavouriteDto;
import com.example.eventfyapp.Responses.CrearEventoResponse;
import com.example.eventfyapp.Responses.ResponseContainer;
import com.example.eventfyapp.Responses.ResponseContainerNoList;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface EventoService {


    //@GET("/properties")
    //Call<ResponseContainer<Propiedad>> getPropertiesNear(@Query("near") String near);

    @GET("/eventos")
    Call<ResponseContainer<Evento>> getEventos();


    @GET("/eventos/fav")
    Call<ResponseContainer<Evento>> getFavEventos();

    @GET("/eventos/auth")
    Call<ResponseContainer<Evento>> getAuthEventos();

    @GET("/eventos/mine")
    Call<ResponseContainer<EventoFavDto>> getMineEventos();

    @POST("/eventos/fav/{id}")
    Call<addFavouriteDto> addFavEvento(@Path("id") String id);

    @DELETE("/eventos/fav/{id}")
    Call<addFavouriteDto> removeFavEvento(@Path("id") String id);

    @GET("/eventos/{id}")
    Call<ResponseContainerNoList<Evento>> getOneEvento(@Path("id") String id);

    @POST("/eventos")
    Call<CrearEventoResponse> addProperty(@Body CrearEventoDto crearPropiedadDto);

    @PUT("/eventos/{id}")
    Call<CrearEventoResponse> editEvento(@Path("id") String id, @Body CrearEventoDto crearEventoDto);


    @DELETE("/eventos/{id}")
    Call<ResponseContainerNoList<Evento>> deleteEvento(@Path("id") String id);


    @GET("/eventos")
    Call<ResponseContainer<Evento>> getEventos(
            @QueryMap Map<String, String> options
    );



}
