package com.example.eventfyapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.eventfyapp.Fragments.LoginFragment;
import com.example.eventfyapp.Fragments.SignUpFragment;

public class SessionActivity extends AppCompatActivity implements LoginFragment.OnFragmentInteractionListener, SignUpFragment.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contenedor, new LoginFragment())
                .commit();

    }


    @Override
    protected void onStart() {
        super.onStart();


 /*       if (UtilUser.getEmail(this) != null) {
            startActivity(new Intent(SessionActivity.this, MainActivity.class));
        }*/
    }

    @Override
    public void navegarRegistro() {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.contenedor, new SignUpFragment())
                .commit();

    }


    @Override
    public void navegarLogin() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.contenedor, new LoginFragment())
                .commit();
    }

}
