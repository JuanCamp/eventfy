package com.example.eventfyapp.Geography.Selector;

import java.util.Map;

public interface GeographyListener {
    public void onGeographySelected(Map<String, String> hm);

}
