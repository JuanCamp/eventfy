package com.example.eventfyapp.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.eventfyapp.Generator.ServiceGenerator;
import com.example.eventfyapp.Generator.UtilUser;
import com.example.eventfyapp.MainActivity;
import com.example.eventfyapp.Model.LoginResponse;
import com.example.eventfyapp.Model.UserDto;
import com.example.eventfyapp.R;
import com.example.eventfyapp.Services.AuthService;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpFragment extends Fragment {

    private static final int READ_REQUEST_CODE = 42;
    private EditText editText_email_registro, editText_password_registro, editText_passwordrep_login, editText_name_registro;
    private Button button_registro, button_registroAlogin;
    private Button btnSubirImagen;
    private RegistroInterface registroInterface;
    Uri uriSelected;
    Context ctx;
    private ImageView ivImagenPerfil;
    private OnFragmentInteractionListener mListener;


    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_sign_up, container, false);

        findViews(view);
        events();

        return view;
    }

    private void events() {

        button_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRegister();
            }
        });

        button_registroAlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registroInterface.navegarLogin();
            }
        });
    }

    private void findViews(View view) {
        editText_name_registro = view.findViewById(R.id.editTextNombreRegistro);
        editText_email_registro = view.findViewById(R.id.editTextEmailRegistro);
        editText_password_registro = view.findViewById(R.id.editTextPasswordRegistro);
        editText_passwordrep_login = view.findViewById(R.id.editTextPasswordRepeat);
        button_registro = view.findViewById(R.id.buttonRegistrar);
        button_registroAlogin = view.findViewById(R.id.buttonRegistroaLogin);
        ivImagenPerfil = view.findViewById(R.id.imageViewPreImgPerfil);
        btnSubirImagen = view.findViewById(R.id.buttonSubirImagen);

        btnSubirImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performFileSearch();
            }
        });

        button_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRegister();
            }
        });

        button_registroAlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.navegarLogin();
            }
        });
    }



    public void doRegister(){
        if (uriSelected != null) {

            AuthService service = ServiceGenerator.createService(AuthService.class);
            ctx=getView().getContext();

            try {
                InputStream inputStream = ctx.getContentResolver().openInputStream(uriSelected);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                int cantBytes;
                byte[] buffer = new byte[1024*4];

                while ((cantBytes = bufferedInputStream.read(buffer,0,1024*4)) != -1) {
                    baos.write(buffer,0,cantBytes);
                }


                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(ctx.getContentResolver().getType(uriSelected)), baos.toByteArray());


                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("picture", "picture", requestFile);


                RequestBody email = RequestBody.create(MultipartBody.FORM, editText_email_registro.getText().toString().trim());
                RequestBody password = RequestBody.create(MultipartBody.FORM, editText_password_registro.getText().toString().trim());
                RequestBody name = RequestBody.create(MultipartBody.FORM, editText_name_registro.getText().toString().trim());

                if (validarString(email) && validarString(password) && validarString(name)) {

                    Call<LoginResponse> callRegister = service.doRegister(body, email, password, name);

                    callRegister.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            if (response.isSuccessful()) {
                                Log.d("Uploaded", "Éxito");
                                Log.d("Uploaded", response.body().toString());
                                UtilUser.setUserInfo(getActivity(), response.body().getUser());
                                startActivity(new Intent(getActivity(), MainActivity.class));
                            } else {
                                Log.e("Upload error", response.errorBody().toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            Log.e("Upload error", t.getMessage());
                        }
                    });

                }else {
                    Toast.makeText(getContext(), "Debes rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }


    public void performFileSearch() {

        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("image/*");

        startActivityForResult(intent, READ_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {

        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                Log.i("Filechooser URI", "Uri: " + uri.toString());
                //showImage(uri);
                Glide
                        .with(this)
                        .load(uri)
                        .into(ivImagenPerfil);

                uriSelected = uri;
                button_registro.setEnabled(true);
            }
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void navegarLogin();

    }

    Boolean validarString (RequestBody texto) {
        return texto != null && texto.toString().length() >0;
    }

    public interface RegistroInterface {
        void navegarLogin();
    }

}
