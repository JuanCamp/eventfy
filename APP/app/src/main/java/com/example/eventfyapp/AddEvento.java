package com.example.eventfyapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventfyapp.Generator.ServiceGenerator;
import com.example.eventfyapp.Generator.TipoAutenticacion;
import com.example.eventfyapp.Generator.UtilToken;
import com.example.eventfyapp.Geography.Geocode;
import com.example.eventfyapp.Geography.Selector.GeographyListener;
import com.example.eventfyapp.Geography.Selector.GeographySelector;
import com.example.eventfyapp.Model.Categoria;
import com.example.eventfyapp.Model.CrearEventoDto;
import com.example.eventfyapp.Model.Evento;
import com.example.eventfyapp.Responses.CrearEventoResponse;
import com.example.eventfyapp.Responses.ResponseContainer;
import com.example.eventfyapp.Responses.ResponseContainerNoList;
import com.example.eventfyapp.Services.CategoriaService;
import com.example.eventfyapp.Services.EventoService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddEvento extends AppCompatActivity implements GeographyListener {

    private TextView tvRegion, tvProvincia, tvMunicipio;
    private EditText editText_title, editText_descripcion, editText_provincia, editText_direccion;
    private Button button_ubicacion, button_add;
    private Spinner spinner_category;
    List<Categoria> categorias;
    private String direccionCompleta;
    private CrearEventoDto crearEventoDto;
    private boolean edit;
    private Evento evento;
    private String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_evento);

        findviews();

        events();

        Bundle b = new Bundle();
        b = getIntent().getExtras();
        if (b != null) {
            edit = true;
            id = b.getString("idevento");
            Log.i("IDEVENTO: ", id);
            setearInformacionEvento(id);
            button_add.setText("Editar Evento");
        }

        getCategorias();


    }

    private void setearInformacionEvento(String idevento) {
        EventoService service = ServiceGenerator.createService(EventoService.class);

        Call<ResponseContainerNoList<Evento>> call = service.getOneEvento(idevento);

        call.enqueue(new Callback<ResponseContainerNoList<Evento>>() {
            @Override
            public void onResponse(Call<ResponseContainerNoList<Evento>> call, Response<ResponseContainerNoList<Evento>> response) {
                if (response.code() != 200) {
                    Toast.makeText(AddEvento.this, "Error al ver Evento", Toast.LENGTH_SHORT).show();
                } else {
                    evento = response.body().getRows();
                    editText_title.setText(evento.getTitulo());
                    editText_descripcion.setText(evento.getDescripcion());
                    editText_direccion.setText(evento.getDireccion());
                    editText_provincia.setText(evento.getProvincia());
                }
            }

            @Override
            public void onFailure(Call<ResponseContainerNoList<Evento>> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(AddEvento.this, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void getCategorias() {

        CategoriaService service = ServiceGenerator.createService(CategoriaService.class);
        Call<ResponseContainer<Categoria>> call = service.getCategorias();

        call.enqueue(new Callback<ResponseContainer<Categoria>>() {
            @Override
            public void onResponse(Call<ResponseContainer<Categoria>> call, Response<ResponseContainer<Categoria>> response) {
                if (response.code() != 200) {
                    Toast.makeText(AddEvento.this, "Fallo al traer categorias", Toast.LENGTH_SHORT).show();
                } else {
                    categorias = response.body().getRows();

                    ArrayAdapter<Categoria> adapter =
                            new ArrayAdapter<>(AddEvento.this, android.R.layout.simple_spinner_dropdown_item, categorias);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinner_category.setAdapter(adapter);
                    spinner_category.setSelection(categorias.size() - 1);


                }
            }

            @Override
            public void onFailure(Call<ResponseContainer<Categoria>> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(AddEvento.this, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public String getLocation(String direccion) throws IOException {
        String loc = Geocode.getLatLong(AddEvento.this, direccion);
        return loc;
    }

    private void findviews() {
        button_ubicacion = findViewById(R.id.button_ubicacion);
        spinner_category = findViewById(R.id.spinner_category);
        button_add = findViewById(R.id.btn_add);
        editText_title = findViewById(R.id.et_title);
        editText_descripcion = findViewById(R.id.et_descripcion);
        editText_provincia = findViewById(R.id.et_provincia);
        editText_direccion = findViewById(R.id.et_direccion);
    }


    private void events() {
        button_ubicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeographySelector gs = new GeographySelector(AddEvento.this);
                gs.setOnGeograpySelectedListener(AddEvento.this);
                FragmentManager fm = getSupportFragmentManager();
                gs.show(fm, "geographySelector");
            }
        });

        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit) {
                    editarEvento();
                } else {
                    subirEvento();
                }

            }
        });
    }

    private void editarEvento() {
        String direccionCompleta = "Calle " + editText_direccion.getText().toString();
        String loc = null;
        try {
            loc = getLocation(direccionCompleta);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Categoria category = (Categoria) spinner_category.getSelectedItem();

        crearEventoDto = new CrearEventoDto(editText_title.getText().toString(),
                editText_descripcion.getText().toString(),
                category.getId(),
                editText_direccion.getText().toString(),
                editText_provincia.getText().toString(),
                loc);

        EventoService service = ServiceGenerator.createService(EventoService.class, UtilToken.getToken(this), TipoAutenticacion.JWT);
        Call<CrearEventoResponse> call = service.editEvento(id, crearEventoDto);

        /*Llamada a editar*/
        call.enqueue(new Callback<CrearEventoResponse>() {
            @Override
            public void onResponse(Call<CrearEventoResponse> call, Response<CrearEventoResponse> response) {
                if (response.code() != 200) {
                    Toast.makeText(AddEvento.this, "Fallo al editar evento", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddEvento.this, "Editado con éxito", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<CrearEventoResponse> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(AddEvento.this, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void subirEvento() {

        String direccionCompleta = editText_direccion.getText().toString() + ", España";
        String loc = null;
        int likes = 0;
        try {
            loc = getLocation(direccionCompleta);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Categoria category = (Categoria) spinner_category.getSelectedItem();

        crearEventoDto = new CrearEventoDto(editText_title.getText().toString(),
                editText_descripcion.getText().toString(),
                category.getId(),
                editText_direccion.getText().toString(),
                editText_provincia.getText().toString(),
                loc,
                likes);

        EventoService service = ServiceGenerator.createService(EventoService.class, UtilToken.getToken(this), TipoAutenticacion.JWT);
        Call<CrearEventoResponse> call = service.addProperty(crearEventoDto);

        call.enqueue(new Callback<CrearEventoResponse>() {
            @Override
            public void onResponse(Call<CrearEventoResponse> call, Response<CrearEventoResponse> response) {
                if (response.code() != 201) {
                    Toast.makeText(AddEvento.this, "Fallo al crear evento", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddEvento.this, "Evento creado!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<CrearEventoResponse> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                Toast.makeText(AddEvento.this, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });


    }


    public void onGeographySelected(Map<String, String> hm) {
        //tvRegion.setText(hm.get(GeographySpain.REGION));
        //tvProvincia.setText(hm.get(GeographySpain.PROVINCIA));
        //tvMunicipio.setText(hm.get(GeographySpain.MUNICIPIO));
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
