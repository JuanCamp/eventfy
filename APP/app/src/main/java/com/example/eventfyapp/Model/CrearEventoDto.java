package com.example.eventfyapp.Model;

import java.util.Arrays;

public class CrearEventoDto {
    private String titulo;
    private String descripcion;
    private String categoriaId;
    private String direccion;
    private String provincia;
    private String loc;
    private int likes;
    private String[] imagenes;

    public CrearEventoDto(String title, String description, double price, String id, String s, String address, String zipcode, String province, String loc) {
        this.titulo = title;
        this.descripcion = description;
        this.direccion = address;
        this.provincia = province;
        this.loc = loc;
        this.imagenes = imagenes;
    }

    public CrearEventoDto(String titulo, String descripcion, String categoriaId, String direccion, String provincia, String[] fotos) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.categoriaId = categoriaId;
        this.direccion = direccion;
        this.provincia = provincia;
        this.loc = loc;
    }

    public CrearEventoDto(String titulo, String descripcion, String categoriaId, String direccion, String provincia, String loc, int likes) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.categoriaId = categoriaId;
        this.direccion = direccion;
        this.provincia = provincia;
        this.loc = loc;
        this.likes = likes;
        this.imagenes = imagenes;
    }

    public CrearEventoDto(String titulo, String descripcion, String categoriaId, String direccion, String provincia, String loc) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.categoriaId = categoriaId;
        this.direccion = direccion;
        this.provincia = provincia;
        this.loc = loc;
        this.imagenes = imagenes;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCategoriaId() {
        return categoriaId;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getLoc() {
        return loc;
    }

    public int getLikes() {
        return likes;
    }

    public String[] getImagenes() {
        return imagenes;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCategoriaId(String categoriaId) {
        this.categoriaId = categoriaId;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public void setImagenes(String[] imagenes) {
        this.imagenes = imagenes;
    }
}
