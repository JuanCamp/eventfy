package com.example.eventfyapp.ViewModels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.eventfyapp.Model.Evento;

import java.util.List;

public class FiltroViewModel extends ViewModel {

    private final MutableLiveData<List<Evento>> listaEventos = new MutableLiveData<>();


    public void selectEventoList(List<Evento> eventos) {
        listaEventos.setValue(eventos);
    }

    public MutableLiveData<List<Evento>> getAll() {
        return listaEventos;
    }
}
