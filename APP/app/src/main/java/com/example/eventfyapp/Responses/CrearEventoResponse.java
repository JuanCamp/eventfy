package com.example.eventfyapp.Responses;

import java.util.List;

public class CrearEventoResponse {

    private String id;
    private UserId userId;
    private String titulo;
    private String descripcion;
    private int like;
    private String categoryId;
    private String direccion;
    private String provincia;
    private String loc;
    private double valoracionMedia;
    public String createdAt;
    public String updatedAt;

    public String getId() {
        return id;
    }

    public UserId getUserId() {
        return userId;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getLike() {
        return like;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getLoc() {
        return loc;
    }

    public double getValoracionMedia() {
        return valoracionMedia;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public void setValoracionMedia(double valoracionMedia) {
        this.valoracionMedia = valoracionMedia;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}


class UserId {
    public String id;
    public String name;
    public String picture;
    public String email;
    public String createdAt;
    public List<String> favs = null;

    public UserId(String id, String name, String picture, String email, String createdAt, List<String> favs) {
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.email = email;
        this.createdAt = createdAt;
        this.favs = favs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public class CrearPropiedadResponse {

        private String id;
        private UserId userId;
        private String titulo;
        private String descripcion;
        private int like;
        private String categoryId;
        private String direccion;
        private String provincia;
        private String loc;
        private double valoracionMedia;
        public String createdAt;
        public String updatedAt;

        public String getId() {
            return id;
        }

        public UserId getUserId() {
            return userId;
        }

        public String getTitulo() {
            return titulo;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public int getLike() {
            return like;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public String getDireccion() {
            return direccion;
        }

        public String getProvincia() {
            return provincia;
        }

        public String getLoc() {
            return loc;
        }

        public double getValoracionMedia() {
            return valoracionMedia;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setUserId(UserId userId) {
            this.userId = userId;
        }

        public void setTitulo(String titulo) {
            this.titulo = titulo;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public void setLike(int like) {
            this.like = like;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public void setDireccion(String direccion) {
            this.direccion = direccion;
        }

        public void setProvincia(String provincia) {
            this.provincia = provincia;
        }

        public void setLoc(String loc) {
            this.loc = loc;
        }

        public void setValoracionMedia(double valoracionMedia) {
            this.valoracionMedia = valoracionMedia;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<String> getFavs() {
        return favs;
    }

    public void setFavs(List<String> favs) {
        this.favs = favs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserId ownerId = (UserId) o;

        if (id != null ? !id.equals(ownerId.id) : ownerId.id != null) return false;
        if (name != null ? !name.equals(ownerId.name) : ownerId.name != null) return false;
        if (picture != null ? !picture.equals(ownerId.picture) : ownerId.picture != null)
            return false;
        if (email != null ? !email.equals(ownerId.email) : ownerId.email != null) return false;
        if (createdAt != null ? !createdAt.equals(ownerId.createdAt) : ownerId.createdAt != null)
            return false;
        return favs != null ? favs.equals(ownerId.favs) : ownerId.favs == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (picture != null ? picture.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (favs != null ? favs.hashCode() : 0);
        return result;
    }
}